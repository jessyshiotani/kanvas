from django.urls import path
from .views import CourseByIdView, CourseView, RegisterStudentView

urlpatterns = [
    path('courses/', CourseView.as_view()),
    path('courses/<int:course_id>/registrations/', RegisterStudentView.as_view()),
    path('courses/<int:course_id>/', CourseByIdView.as_view())
  
]