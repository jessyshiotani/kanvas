from django.shortcuts import get_object_or_404
from rest_framework.authentication import TokenAuthentication

from rest_framework.response import Response
from rest_framework.views import APIView
from courses.serializers import CoursesSerializer
from rest_framework import status
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from courses.models import Course
from users.permissions import IsInstructorPermission


class CourseView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructorPermission]

    def post(self, request):

        course_data = request.data
        try:
            course = Course.objects.get(**course_data)
            return Response({'error': 'Course with this name already exists'}, status=status.HTTP_400_BAD_REQUEST)

        except:
            course = Course.objects.create(**course_data)
            serialized = CoursesSerializer(course)
            return Response(serialized.data, status=status.HTTP_201_CREATED)

    def get(self, request):

        courses = Course.objects.all()
        serialized = CoursesSerializer(courses, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)


class CourseByIdView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructorPermission]

    def put(self, request, course_id=''):
        data = request.data['name']
        course = Course.objects.get(id=course_id)

        course.name = data
        course.save()

        serializer = CoursesSerializer(course)

        return Response(serializer.data, status=status.HTTP_200_OK)


    def delete(self, request, course_id=''):
        course = get_object_or_404(Course, id=course_id)
        course.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def get(self, request, course_id=''):
        if course_id:
                course = get_object_or_404(Course, id=course_id)
                serializer = CoursesSerializer(course)
                return Response(serializer.data, status=status.HTTP_200_OK)

        courses = Course.objects.all()
        serialized = CoursesSerializer(courses, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)


class RegisterStudentView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructorPermission]

    def put(self, request, course_id=''):
        data = request.data['users']
        
        try:
            course = Course.objects.get(id=course_id)
        except ObjectDoesNotExist:
            return Response({'errors': 'invalid course_id'}, status=status.HTTP_404_NOT_FOUND)

        users_list = []
        for students_id in data:
            try:
                user = User.objects.get(id=students_id)

            except ObjectDoesNotExist:
                return Response({"errors": "invalid user_id list"}, status=status.HTTP_404_NOT_FOUND)

            if user.is_staff == False and user.is_superuser == False:
                users_list.append(user)

            else:
                return Response ({"errors": "Only students can be enrolled in the course."}, status=status.HTTP_400_BAD_REQUEST)
        
        course.users.set(users_list)

        serializer = CoursesSerializer(course)
    
        return Response(serializer.data, status=status.HTTP_200_OK)



  



