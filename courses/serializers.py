from rest_framework import serializers

class UserStudentSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField()

class CoursesSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()

    users = UserStudentSerializer(many=True)

