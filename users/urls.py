from django.urls import path
from .views import LoginUser, RegisterUser

urlpatterns = [
    path('login/', LoginUser.as_view()),
    path('accounts/', RegisterUser.as_view()),
]