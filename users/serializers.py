from rest_framework import serializers

# from courses.serializers import CoursesSerializer
from rest_framework import serializers

class UsersSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField()
    is_superuser = serializers.BooleanField()
    is_staff = serializers.BooleanField()
   
 