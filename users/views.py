from django.http.response import HttpResponseBadRequest
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from users.serializers import UsersSerializer
from django.contrib.auth.models import User
from rest_framework.authentication import authenticate
from rest_framework.authtoken.models import Token
# Create your views here.


class RegisterUser(APIView):
    def post(self, request):
        data = request.data

        user_exists = User.objects.filter(username=data['username'])

        if user_exists:
            return Response({'error': 'Account already exists'}, status=status.HTTP_409_CONFLICT)
        
        else:
            user = User.objects.create_user(**data)

            serialized = UsersSerializer(user)

            return Response(serialized.data, status=status.HTTP_201_CREATED)


class LoginUser(APIView):
    def post(self, request):
        try:
            username = request.data['username']
            password = request.data['password']

        except KeyError:
            return HttpResponseBadRequest()

        user = authenticate(username=username, password=password)

        if user:
            token = Token.objects.get_or_create(user=user)[0]
            return Response({"token": token.key})
        
        return Response({"error": 'Invalid username or password'}, status=status.HTTP_401_UNAUTHORIZED)














