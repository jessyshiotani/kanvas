from rest_framework.permissions import BasePermission

class IsStudentPermission(BasePermission):
    def has_permission(self, request, view):
        if bool(request.user.is_superuser and request.user.is_staff) == False:
            return True


class IsInstructorPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method == "GET":
            return True

        if bool(request.user.is_superuser and request.user.is_staff) == True:
            return True


class IsFacilitatorPermission(BasePermission):
    def has_permission(self, request, view):
        if bool(request.user.is_superuser) == False and bool(request.user.is_staff) == True:
            return True


class IsFacilitatorOrInstructorPermission(BasePermission):
    def has_permission(self, request, view):
        if bool(request.user.is_staff) == True:
                return True
