from django.urls import path
from .views import SubmissionsByIdView, SubmissionsView

urlpatterns = [
    path('submissions/', SubmissionsView.as_view()),
    path('submissions/<int:submission_id>/', SubmissionsByIdView.as_view())
]