from django.db import models
from django.utils.translation import activate

from django.contrib.auth.models import User
from activities.models import Activity

# Create your models here.
class Submission(models.Model):
    grade = models.FloatField()
    repo = models.CharField(max_length=425)

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='submissions')
    activity = models.ForeignKey('activities.Activity', on_delete=models.CASCADE, related_name='submissions')
