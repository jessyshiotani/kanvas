from rest_framework import serializers

class SubmissionsSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    grade = serializers.FloatField()
    repo = serializers.CharField()
    
    user_id = serializers.IntegerField(read_only=True)
    activity_id = serializers.IntegerField(read_only=True)
   