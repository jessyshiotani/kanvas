
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from users.permissions import IsFacilitatorOrInstructorPermission, IsStudentPermission
from submissions.models import Submission
from submissions.serializers import SubmissionsSerializer
from rest_framework import status
from django.core.exceptions import ObjectDoesNotExist

class SubmissionsByIdView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsFacilitatorOrInstructorPermission]

    def put(self, request, submission_id=''):
        data = request.data
        try:
            submission = Submission.objects.get(id=submission_id)

        except ObjectDoesNotExist:
            return Response({'error': 'This activity does not exist'}, status=status.HTTP_400_BAD_REQUEST)
    
        submission.grade = data['grade']
        serialized = SubmissionsSerializer(submission)
        submission.save()

        return Response(serialized.data, status=status.HTTP_200_OK)
        


class SubmissionsView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsStudentPermission]

    def get(self, request):
     
        submissions = Submission.objects.all()
        serialized = SubmissionsSerializer(submissions, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)

        


    