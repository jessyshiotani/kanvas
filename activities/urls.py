from django.urls import path

from activities.models import Activity
from .views import ActivitiesByIdView, ActivitiesView, SubmissionsStudentView


urlpatterns = [
    path('activities/', ActivitiesView.as_view()),
    path('activities/<int:activity_id>/submissions/', SubmissionsStudentView.as_view()),
    path('activities/<int:activity_id>/', ActivitiesByIdView.as_view())
]