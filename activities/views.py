from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from activities.models import Activity
from rest_framework.response import Response
from rest_framework import status
from activities.serializers import ActivitiesSerializer
from submissions.serializers import SubmissionsSerializer
from submissions.models import Submission
from users.permissions import IsFacilitatorOrInstructorPermission, IsStudentPermission
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
# Create your views here.
class ActivitiesView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsFacilitatorOrInstructorPermission]

    def post(self, request):
        data = request.data

        if Activity.objects.filter(title=data['title']).exists():
            return Response({'error': 'Activity with this name already exists'}, status=status.HTTP_400_BAD_REQUEST)
      
        activity = Activity.objects.create(**data)
        serialized = ActivitiesSerializer(activity)
        return Response(serialized.data, status=status.HTTP_201_CREATED)


    def get(self, request):
        activities = Activity.objects.all()
        serialized = ActivitiesSerializer(activities, many=True)

        return Response(serialized.data, status=status.HTTP_200_OK)


class ActivitiesByIdView(APIView):
    def put(self, request, activity_id=''):
        data = request.data

        activity = get_object_or_404(Activity, id=activity_id)

        if  activity.submissions.count() > 0:
            return Response({'error': 'You can not change an Activity with submissions'}, status=status.HTTP_400_BAD_REQUEST)

        activity.title = data['title']
        serialized = ActivitiesSerializer(activity)
        activity.save()

        return Response(serialized.data, status=status.HTTP_200_OK)



class SubmissionsStudentView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsStudentPermission]

    def post(self, request, activity_id=''):
        data = request.data

        try:
            user = request.user.id
            activity_id = Activity.objects.get(id=activity_id)     

        except ObjectDoesNotExist:
            return Response({'error': 'This activity does not exist'}, status=status.HTTP_400_BAD_REQUEST)

        submission = Submission.objects.create(**data, activity_id=activity_id.id, user_id=user)
        serialized = SubmissionsSerializer(submission)

        return Response(serialized.data, status=status.HTTP_201_CREATED)
        
        