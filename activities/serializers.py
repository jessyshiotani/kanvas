from rest_framework import serializers
from submissions.serializers import SubmissionsSerializer



class ActivitiesSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    title = serializers.CharField()
    points = serializers.FloatField()

    submissions = SubmissionsSerializer(many=True)