# Kanvas - API

## 💻 Sobre

Sistema desenvolvido para uma plataforma de Cursos, com o objetivo de facilitar o controle, a criação e o cadastramento de alunos, instrutores e facilidores. Sendo possível criar cursos, cadastrar alunos nos cursos criados, assim como criar e listar atividades, pontuações e envio de trabalhos.

## 🛠 Instalação

_# Este passo é para baixar o projeto_

**git clone https://gitlab.com/<your_user>/kanvas.git**

Entrar na pasta, criar um ambiente virtual e instalar as dependências:

_# Entrar na pasta_

**cd kanvas**

_# Criar um ambiente virtual_

**python3 -m venv venv**

_# Instalar as dependências_

**pip install -r requirements.txt**

Rodar as migrations para que o banco de dados e as tabelas sejam criadas:

_# Rodar as migrations_

**./manage.py migrate**

_# Rodar o servidor_

**./manage.py runserver**

## 🚀 Utilização

Para utilizar este sistema, é necessário utilizar um API Client, como o Insomnia.

## Criação de Usuários

**POST** /api/accounts/

Rota para o cadastro dos usuários.

Exemplo do corpo da requisição:

```json
{
  "username": "student",
  "password": "1234",
  "is_superuser": false,
  "is_staff": false
}
```

Exemplo de retorno:

RESPONSE STATUS -> HTTP 201 (Created)

```json
{
  "id": 1,
  "username": "student",
  "is_superuser": false,
  "is_staff": false
}
```

RESPONSE STATUS -> HTTP 409 (Conflict)
Caso haja a tentativa de criação de um usuário que já está cadastrado.

#

## Autenticação:

**POST** /api/login/

Rota para o login do usuário.

```json
{
  "username": "student",
  "password": "1234"
}
```

Exemplo de retorno com o token de autenticação:

RESPONSE STATUS -> HTTP 200 (ok)

```json
{
  "token": "dfd384673e9127213de6116ca33257ce4aa203cf"
}
```

RESPONSE STATUS -> HTTP 401 (Unauthorized)
Caso haja a tentativa de login de uma conta que ainda não tenha sido criada.

RESPONSE STATUS -> HTTP 403 (Forbidden)
Foi fornecido um token de outro tipo de usuário, diferente ao exigido que para acessar ao endpoint.

#

## Criação de Cursos:

**POST** /api/courses/

Rota criação e cadastramento de novos cursos.

Header -> Authorization: Token (_token-do-instrutor_)

```json
{
  "name": "Node"
}
```

Exemplo de retorno:

RESPONSE STATUS -> HTTP 201 (Created)

```json
{
  "id": 1,
  "name": "Node",
  "users": []
}
```

RESPONSE STATUS -> HTTP 400 (Bad Request)
Caso haja a tentativa de criação de um curso já existente.

#

## Atualizar o nome do curso:

**PUT** /api/courses/<course_id>/

Recebe os mesmos dados que o POST e formato da resposta é o mesmo.

RESPONSE STATUS -> HTTP 200 (ok)

---

## Atualizar lista de estudantes matriculados em um curso:

**PUT** /api/courses/<course_id>/registrations/

Deve ser informada uma lista de id's de estudantes para serem matriculados no curso.

Header -> Authorization: Token (_token-do-instrutor_)

```json
{
  "user_ids": [3, 4, 5]
}
```

Exemplo de retorno:

RESPONSE STATUS -> HTTP 200 (ok)

```json
{
  "id": 1,
  "name": "Node",
  "users": [
    {
      "id": 3,
      "username": "student1"
    },
    {
      "id": 4,
      "username": "student2"
    },
    {
      "id": 5,
      "username": "student3"
    }
  ]
}
```

RESPONSE STATUS -> HTTP 400 (Bad Request)
Caso haja a tentativa de matricular no curso, usuáios que não sejam estudantes.

RESPONSE STATUS -> HTTP 404 (Not Found)
Caso seja informado id de curso inválido.

---

## Obter lista de cursos e alunos:

**GET** /api/courses/

Lista de cursos, mostrando cada aluno inscrito no curso.

Header -> Authorization: Pode ser acessado por qualquer usuário.

RESPONSE STATUS -> HTTP 200 (ok)

```json
[
  {
    "id": 1,
    "name": "Node",
    "users": [
      {
        "id": 3,
        "username": "student1"
      }
    ]
  },
  {
    "id": 2,
    "name": "Django",
    "users": []
  },
  {
    "id": 3,
    "name": "React",
    "users": []
  }
]
```

---

## Filtrar lista de cursos:

**GET** /api/courses/<course_id>/

Traz o curso filtrado pelo id informado na url.

Header -> Authorization: Pode ser acessado por qualquer usuário.

RESPONSE STATUS -> HTTP 200 (ok)

```json
{
  "id": 1,
  "name": "Node",
  "users": [
    {
      "id": 3,
      "username": "student1"
    }
  ]
}
```

RESPONSE STATUS -> HTTP 404 (Not Found)
Caso seja informado id de curso inválido.

---

## Deletar Curso:

**DELETE** /api/courses/<course_id>/

Faz a exclusão do curso no sistema.

Header -> Authorization: Token (_token-do-instrutor_)

RESPONSE STATUS -> HTTP 204 (No Content)

---

## Criação de uma Atividade:

**PUT** /api/courses/<course_id>/registrations/

Cadastramento de uma atividade no sistema pelos facilitadores ou instrutores para que os alunos possam fazer suas submissões.

Header -> Authorization: Token (_token-do-facilitador_ ou _token-do-instrutor_)

```json
{
  "title": "Kenzie Pet",
  "points": 10
}
```

Exemplo de retorno:

RESPONSE STATUS -> HTTP 201 (Created)

```json
{
  "id": 1,
  "title": "Kenzie Pet",
  "points": 10,
  "submissions": []
}
```

RESPONSE STATUS -> HTTP 400 (Bad Request)
Caso haja a tentativa de cadastrar, no sistema, uma atividade já existente.

---

## Listar Atividades:

**GET** /api/activities/

Lista todas as atividades e submissões feitas para aquela atividade.

Header -> Authorization: Token (_token-do-facilitador_ ou _token-do-instrutor_)

RESPONSE STATUS -> HTTP 200 (ok)

```json
[
    {
        "id": 1,
        "title": "Kenzie Pet",
        "points": 10,
        "submissions": [
            {
                "id": 1,
                "grade": 10,
                "repo": "http://gitlab.kenzie_pet",
                "user_id": 3,
                "activity_id": 1
            }
        ]
    },
    {
        "id": 2,
        "title": "Kanvas",
        "points": 10,
        "submissions": [
            {
                "id": 2,
                "grade": 8,
                "repo": "http://gitlab.com/kanvas",
                "user_id": 4,
                "activity_id": 2
            }
        ]
    },
    ...
]
```

RESPONSE STATUS -> HTTP 400 (Bad Request)
Caso haja a tentativa de cadastrar, no sistema, uma atividade já existente.

---

## Atualizar Atividade:

**UPDATE** /api/activities/<activity_id>/

Recebe os mesmos dados que o POST e formato da resposta é o mesmo.

Header -> Authorization: Token (_token-do-estudante_)

RESPONSE STATUS -> HTTP 200 (ok)

RESPONSE STATUS -> HTTP 404 (Not Found)
Caso seja informado id de atividade inválida.

RESPONSE STATUS -> HTTP 400 (Bad Request)
Caso a atividade sendo alterada já possui alguma submissão e para evitar que sofra alteração no nome ou na pontuação.

---

## Submissão de uma Atividade:

**POST** /api/activities/<activity_id>/submissions/

Faz submissão da atividade pelo estudante, informando o id da atividade na url.

Header -> Authorization: Token (_token-do-estudante_)

```json
{
  "grade": 10, // Esse campo é opcional
  "repo": "http://gitlab.com/kenzie_pet"
}
```

Exemplo de retorno:

RESPONSE STATUS -> HTTP 201 (Created)

```json
{
  "id": 7,
  "grade": null,
  "repo": "http://gitlab.com/kenzie_pet",
  "user_id": 3,
  "activity_id": 1
}
```

---

## Atualizando nota de Submissão:

**POST** /api/activities/<activity_id>/submissions/

Atualiza nota de submissão do aluno.

Header -> Authorization: Token (_token-do-facilitador_ ou _token-do-instrutor_)

```json
{
  "grade": 10
}
```

Exemplo de retorno:

RESPONSE STATUS -> HTTP 200 (ok)

```json
{
  "id": 3,
  "grade": 10,
  "repo": "http://gitlab.com/kenzie_pet",
  "user_id": 3,
  "activity_id": 1
}
```

---

## Lista as Submissões:

**POST** /api/activities/<activity_id>/submissions/

Retorna apenas as submissões daquele estudante baseadas no token informado no sistema.

Header -> Authorization: Token (_token-do-estudante_)

RESPONSE STATUS -> HTTP 200 (ok)

```json
[
  {
    "id": 2,
    "grade": 8,
    "repo": "http://gitlab.com/kanvas",
    "user_id": 4,
    "activity_id": 2
  },
  {
    "id": 5,
    "grade": null,
    "repo": "http://gitlab.com/kmdb2",
    "user_id": 4,
    "activity_id": 1
  }
]
```

Retorna todas as submissões dos estudantes.

Header -> Authorization: Token (_token-do-facilitador_ ou _token-do-instrutor_)

RESPONSE STATUS -> HTTP 200 (ok)

```json
[
  {
    "id": 1,
    "grade": 10,
    "repo": "http://gitlab.com/kenzie_pet",
    "user_id": 3,
    "activity_id": 1
  },
  {
    "id": 2,
    "grade": 8,
    "repo": "http://gitlab.com/kanvas",
    "user_id": 4,
    "activity_id": 2
  },
  {
    "id": 3,
    "grade": 4,
    "repo": "http://gitlab.com/kmdb",
    "user_id": 5,
    "activity_id": 3
  },
  {
    "id": 4,
    "grade": null,
    "repo": "http://gitlab.com/kmdb2",
    "user_id": 5,
    "activity_id": 3
  }
]
```

---
